CC=gcc
LD=gcc
CFLAGS=-g -Wall -Werror -pedantic -ansi -O2 
#CFLAGS=-g -Wall -Werror -pedantic -ansi -O2 -DLESS_PROBLEMATIC
LDFLAGS=

PROG=ndeql

default:all

clean:
	find -name '*.o' -delete
	find -name '*~' -delete
	rm -f $(PROG)

all: $(PROG)

ndeql: ndeql.o
	$(LD) $(LDFLAGS) -o $@ $^

%.o : %.c
	$(CC) $(CFLAGS) -o $@ -c $<
